﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour
{
    [SerializeField]
    private Transform cameraArm;
    [SerializeField]
    private Transform Player;

    public float speed = 3f;
    public float runspeed = 5f;
    public float backspeed = 1f;
    public float jumpPower = 10f;

    private float rotateSpeed = 5f;
    private Rigidbody rigidbody;

    private Vector2 moveInput;
    private Animator animator;
    private bool isJumping;
    private bool isRunning;
    private bool isWalking;
    private bool isBack;

    float h;
    float v;

    // Start is called before the first frame update
    void Start()
    {
        animator = Player.GetComponent<Animator>();
        rigidbody = Player.GetComponent<Rigidbody>();
    }
    //void Awake()
    //{
    //    rigidbody = GetComponent<Rigidbody>();
    //    animator = GetComponent<Animator>();
    //}
    // Update is called once per frame
    void Update() // 키 입력은 Update
    {
        LookAround();
        Walk();
        if (Input.GetKeyDown(KeyCode.Space))
        {
            isJumping = true;
        }
        if (Input.GetKey(KeyCode.LeftShift))
        {
            isRunning = true;
        }
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            isRunning = false;
        }
        if (Input.GetKey(KeyCode.Z))
        {
            isBack = true;
        }
        if (Input.GetKeyUp(KeyCode.Z))
        {
            isBack = false;
        }

        Run();
        RunBack();
        Jump();
        AnimationUpdate();
    }

    void FixedUpdate() // 물리적 처리는 FixedUpdate
    {

        // Turn();
    }
    private void LookAround()
    {
        moveInput = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        isWalking = moveInput.magnitude != 0;

        Vector2 mouseDelta = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
        Vector3 camAngle = cameraArm.rotation.eulerAngles;
        float x = camAngle.x - mouseDelta.y;
        if (x < 180f)
        {
            x = Mathf.Clamp(x, -1f, 70f);
        }
        else
        {
            x = Mathf.Clamp(x, 335f, 361f);
        }
        cameraArm.rotation = Quaternion.Euler(x, camAngle.y + mouseDelta.x, camAngle.z);
    }

    void AnimationUpdate()
    {
        if (moveInput.magnitude == 0)
        {
            animator.SetBool("isRun", false);
            animator.SetBool("isWalk", false);
            animator.SetBool("isRunBack", false);
        }
    }
    /*   void Walk(float h, float v)
       {
           if (!isWalking)
           {
               animator.SetBool("isWalk", false);
               return;
           }
           animator.SetBool("isWalk", true);
           movement.Set(h, 0, v);
           movement = movement.normalized * speed * Time.deltaTime;
           rigidbody.MovePosition(transform.position + movement);
       }
    */
    void Walk()
    {
        Debug.DrawRay(cameraArm.position, new Vector3(cameraArm.forward.x, 0f, cameraArm.forward.z).normalized, Color.red);
        if (isWalking)
        {
            animator.SetBool("isWalk", true);
            Vector3 lookForward = new Vector3(cameraArm.forward.x, 0f, cameraArm.forward.z).normalized;
            Vector3 lookRight = new Vector3(cameraArm.right.x, 0f, cameraArm.right.z).normalized;
            Vector3 moveDir = lookForward * moveInput.y + lookRight * moveInput.x;

            Player.forward = lookForward;
            transform.position += moveDir * Time.deltaTime * speed;
        }
        else
        {
            animator.SetBool("isWalk", false);
        }
    }
    void Run()
    {
        if (!isRunning)
        {
            animator.SetBool("isRun", false);
            return;
        }
        animator.SetBool("isRun", true);
        Vector3 lookForward = new Vector3(cameraArm.forward.x, 0f, cameraArm.forward.z).normalized;
        Vector3 lookRight = new Vector3(cameraArm.right.x, 0f, cameraArm.right.z).normalized;
        Vector3 moveDir = lookForward * moveInput.y + lookRight * moveInput.x;
        Player.forward = lookForward;
        transform.position += moveDir * Time.deltaTime * runspeed;

    }
    void RunBack()
    {
        if (!isBack)
        {
            animator.SetBool("isRunBack", false);
            return;
        }
        animator.SetBool("isRunBack", true);
        Vector3 lookForward = new Vector3(cameraArm.forward.x, 0f, cameraArm.forward.z).normalized;
        Vector3 lookRight = new Vector3(cameraArm.right.x, 0f, cameraArm.right.z).normalized;
        Vector3 moveDir = lookForward * moveInput.y + lookRight * moveInput.x;
        Player.forward = lookForward;
        transform.position += moveDir * Time.deltaTime * backspeed;

    }
    void Jump()
    {
        if (!isJumping)
        {
            return;
        }
        animator.SetTrigger("isJump");
        rigidbody.AddForce(Vector3.up * jumpPower, ForceMode.Impulse);
        isJumping = false;
    }

    //void Turn()
    //{
    //    if (moveInput.magnitude == 0)
    //    {
    //        return;
    //    }
    //    if (!isBack)
    //    {
    //        Quaternion newRotation = Quaternion.LookRotation(movement);
    //        rigidbody.rotation = Quaternion.Slerp(rigidbody.rotation, newRotation, rotateSpeed * Time.deltaTime);
    //    }
    //}
}
