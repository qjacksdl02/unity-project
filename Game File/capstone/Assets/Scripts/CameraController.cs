﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    public Transform camera;

    public Transform player;

    private float sensitivity = 10.0f;
    public float movespeed = 10.0f;
    private float dist = 5f;
    private float h = 0.0f;
    private float v = 0.0f;
    private float r = 0.0f;
    private float height;
    public float rotSpeed = 50.0f;
    // Start is called before the first frame update
    void Start()
    {
        //        camera.transform.position = player.position - (1 * Vector3.forward * dist) + (Vector3.up * (dist + 2f));

        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        h = Input.GetAxis("Horizontal");
        v = Input.GetAxis("Vertical");

        r += Input.GetAxisRaw("Mouse X");
        height -= Input.GetAxisRaw("Mouse Y") ;

        float scroll = Input.GetAxis("Mouse ScrollWheel") * 10.0f;
        
        if(movespeed > 1000.0f)
        {
            movespeed = 1000.0f;
        }
        else if (movespeed < 10.0f)
        {
            movespeed = 10.0f;
        }
        else
        {
            movespeed += scroll;
        }

        Vector3 moveDir = (Vector3.forward * v) + (Vector3.right * h);
        camera.transform.localEulerAngles = new Vector3(height, r, 0f)*sensitivity;
        camera.Translate(moveDir.normalized * (movespeed + 2.0f) * Time.deltaTime, Space.Self);
    }
}

