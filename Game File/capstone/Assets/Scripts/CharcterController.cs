﻿/*
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharcterController : MonoBehaviour
{
    [SerializeField]
    private float walkSpeed = 1.5f;

    private Rigidbody rigid;
    private float camerRotationLimit = 30f;
    private float currentCameraRotationX = 0;

    private bool isGrounded;
    private bool isJumping;
    private bool isRun;
    private bool isWalk;
    private float ri;
    private float fr;

    private Vector3 lastPos;
    private Vector3 movement;
    [SerializeField]
    private float sensitivity = 1.5f;


    private Animator animator;

    private CapsuleCollider capsuleCollider;


    public int JumpPower;
    public Camera camera;
    public GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        rigid = GetComponent<Rigidbody>();
        isJumping = false;
        animator = GetComponent<Animator>();
        capsuleCollider = GetComponent<CapsuleCollider>();

        camera.transform.position = player.transform.position - (1 * Vector3.forward * 1f) + (Vector3.up * (2f));
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        //        MoveCheck();

        IsGround();
        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {
            Jump();
        }

        CameraRotation();
        CharcterRotation();
    }

    private void MoveCheck()
    {
        if (!isRun && isGrounded)
        {
            if (Vector3.Distance(lastPos, transform.position) >= 0.01f)
            {
                isWalk = true;
                animator.SetBool("isWalk", true);
            }
            else
            {
                isWalk = false;
                animator.SetBool("isWalk", false);
            }
            lastPos = transform.position;
        }
    }
    private void RunCheck()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            animator.SetBool("isRun", true);
            walkSpeed = 2f;
        }
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            animator.SetBool("isRun", false);
            walkSpeed = 1.5f;
            Debug.Log("cancel");
        }
    }

    private void Move()
    {
        ri = Input.GetAxisRaw("Horizontal");
        fr = Input.GetAxisRaw("Vertical");
        if (ri != 0 || fr != 0)
        {
            animator.SetBool("isWalk", true);
        }
        else
        {
            animator.SetBool("isWalk", false);
        }
        RunCheck();

        Vector3 rightMove = transform.right * ri;
        Vector3 frontMove = transform.forward * fr;

        movement = (rightMove + frontMove).normalized * walkSpeed * Time.deltaTime;

        rigid.MovePosition(transform.position + movement);
    }

    private void Turn()
    {
        if (horizontalMove == 0 && verticalMove == 0)
        {

        }
        Quaternion newRotation = Quaternion.LookRotation(movement);
        rigid.MoveRotation(newRotation);
    }


    private void CharcterRotation()
    {
        float y = Input.GetAxisRaw("Mouse X");
        Vector3 rotationY = new Vector3(0f, y, 0f) * sensitivity;
        //rigid.MoveRotation(rigid.rotation * Quaternion.Euler(rotationY));
        transform.Rotate(Vector3.up * sensitivity * y);
    }

    private void Jump()
    {
        if (!isJumping)
        {
            isJumping = true;
            animator.SetBool("isJump", true);
            rigid.AddForce(Vector3.up * JumpPower, ForceMode.Impulse);
        }
        else
        {
            return;
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        // 바닥에 닿으면
        if (collision.gameObject.CompareTag("Ground"))
        {
            isJumping = false;
        }
    }
    private void IsGround()
    {
        isGrounded = Physics.Raycast(transform.position, Vector3.down, capsuleCollider.bounds.extents.y + 0.1f);
    }

    private void CameraRotation()
    {
        float x = Input.GetAxisRaw("Mouse Y");

        currentCameraRotationX -= x * sensitivity;
        currentCameraRotationX = Mathf.Clamp(currentCameraRotationX, -camerRotationLimit, camerRotationLimit);

        camera.transform.localEulerAngles = new Vector3(currentCameraRotationX, 0f, 0f);
    }
}
*/