gitlab 사용법

1. git lab 계정 생성하기
 - Google ID 와 연동하여 생성하는 것이 편함.
 - GitHub를 쓰는 사람이라면 GitHub를 이용하여 로그인 하는것도 가능

2. git lab 프로젝트 생성하기
 - New project를 클릭하여 프로젝트를 생성
 - Project name에 원하는 프로젝트 이름을 적기.
 - Visibility Level은 private 또는 Public으로 할지 선택.
 - README는 따로 체크하지 않고 Create Project를 하고 생성하여도 상관 없다.

3. Sourcetree 설치.
 - Sourcetree를 이용하는 이유??
  i. 기존에는 GitBash를 사용하여 명령어를 입력해 원격 레파지토리에 소스를 등록해야함.
  ii. Git을 그래픽화 시켜 따로 명령어를 입력할 필요 없이 클릭만으로 Git작업 수행가능.

 - https://www.sourcetreeapp.com/ 에 들어가 OS에 맞는 버전 다운로드
 - 계정등록은 Atlassian 계정으로 하면 Google 계정으로도 등록할 수 있다. (아무거나 상관x)
 - SSH키가 존재하면 등록하고 없다면 아니오.
 - 설치완료

4. Sourcetree 이용하기
 - Gitlab에서 만들었던 프로젝트를 들어가 Clone에서 URL을 copy한다.
 - 그 후 Sourcetree에서 Clone을 누르고 소스 경로에 복사한 URL을 붙여넣는다.
 - 만약 저장될 장소를 변경하고싶다면 탐색에서 저장할 위치를 설정해준다.
 - 그 후 클론을 하면 Gitlab의 프로젝트의 원격 레파지토리가 생성된다.

5. 커밋, Pull, Push, 패치, 브랜치, 병합 등 사용법
 - 커밋 
  : 커밋을 하기전 Gitlab의 프로젝트를 Pull하여 받아온다. 그 후 작업을 수행하고 나서 변경된 사항들을 모두 스테이지에 올린 후 어떠한 변경사항이 있는지 내용을 작성하고 커밋.
  (처음 커밋할 경우 계정을 연결하라는 메시지가 뜰 수 있음. 계정 연결하고 커밋수행)
 - Pull
  : Gitlab에 있는 프로젝트를 원격 레파지토리에 받아오는 것.
 - Push
  : 커밋하고 난 후 Gitlab (origin) 프로젝트에 파일들을 올리는 것.
 - 패치
  : 원격 저장소에 저장되어 있는 내용을 다운받으려면 먼저 변경된 내용이 있는지 확인.
   모든 원격 저장소에서 가져오기 선택 후 확인 그 후에 Pull 버튼에 숫자로 표시된다.
   Pull 버튼을 누르고 확인을 누르면 가져오기가 진행되고 성공적으로 원격 저장소에 있던 파일이 사용자의 폴더에 다운로드 된다.

6. 협업하기.
 - Brench, merge
  : 단어 그대로 가지를 뻗어 나가는 것이다. 원본에서 브랜치를 하여 즉 copy본을 따서 작업을 수행하고, 완료하면 다시 merge를 하여 원본에 추가하는 방법이다.
  협업이 가장 좋은 예이다. 하나의 프로젝트를 팀원들과 수행할때, 프로젝트를 브랜치 하여서
  각자의 파트별로 작업을 수행한 후 나중에 merge하여 하나의 프로젝트를 완성시키는 것이다.
 - Pull Request
  :하나의 프로젝트로 합치기전에 팀원중 한명이 작업하던 것을 다른 사람들의 브랜치에 합치고 싶은경우 Pull Request를 하여 합칠 수 있다. 이 경우에 A라는 팀원이 B라는 팀원의 브랜치에 합치고 싶다고 A가 B에게 Pull Request를 보내 B가 A의 작업을 보고 판단하여 합칠것인지 말것인지를 결정한다. 수락 방법은 Merge Pull Request를 누르면된다.
  주의) C에게 Pull Request를 보내기 전에, B는 A의 코드도 추가되어 기존 상태가 달라졌을 것이다. B와 C를 바로 Merge한다면 서로의 코드가 충돌될 수 있으므로 먼저 C의 최신 커밋에서 Merge를 한 후 Conflicts가 일어난다면 수동으로 해결하고 commit+push 한 후에 C에게 Pull Request를 한다. 이러한 과정을 '선 Merge 후 Pull Request'라고 한다.
 - Releases
 : 출시 및 배포하기 위한 단계이다. 모든 팀원들의 브랜치들이 완벽하게 Merge되고난 후 Master 브랜치로 Pull Request를 보내고 Merge를 한다. 그 후 저장소에서 Releases를 누르고 업데이트의 버전과 세부 내용을 적은 후 Publish Release 버튼을 누른다.

7. Fork를 이용한 협업
 - Open Source인 경우
 : 위의 프로젝트를 오픈소스로 올려 다른 사람들이 사용할 수 있도록 한 후 실제 사용자가 기능을 더 추가하면 좋겠다는 생각이 든 경우, 사용자는 이 프로젝트를 Fork하여 기능을 추가해서 기여를 할 수 있다.
   1. 프로젝트를 Fork하여 사용자의 계정으로 리포지토리가 복사된다.
   2. 원격 레포지토리를 로컬에 받아오고 추가 기능을 커밋한다.
   3. Master말고 기능의 최신 커밋부터 작업해야 나중에 Pull Request를 기능으로 보낼 수 있다. (이유 : Master는 배포 가능한 코드만 들어가야한다는 규칙이 있는 경우)
   4. Pull Request를 보내기 전 원본 저장소에 변경 사항이 있는지 확인
   (Remotes -> New Remote로 원본 저장소의 url을 upstream이라는 이름으로 추가)
   5. 2가지 경우가 발생
    - 그냥 Pull Request를 보내면 충돌이 일어날 수 있다. 그러므로 선 Merge 후 Pull Request를 한다.
    - 깔끔한 코드 히스토리를 남기기 위해 Rebase 사용
     - Rebase란?
      - 단어 그대로 다시 베이스를 정하는 것인데, a->b->c커밋 순으로 진행할 때 c커밋에서 따서 a커밋과 merge한다면 충돌이 일어날 수 있다.
       그래서 a커밋에서 브랜치를 딴 것처럼 히스토리를 조작하는 것이다.
      - 주의) force push를 해주어야 한다.(히스토리를 망치는 push이므로)
      - 다른 사람이 이 브랜치를 수정하고 있지 않을 때에만 실행
     - fetch를 눌러 원격과 소스트리를 동기화
     - 바로 Merge가능
    6. fork된 리포지토리에서 Pull Request를 누른다.
    7. base fork를 원본의 추가하고자 하는 브랜치, head fork를 수정자 master 브랜치로 한다.
    8. 원본개발자가 Merge를 하면 원본 프로젝트의 contributer로 등록된다.
    9. fork를 진행한 원격 저장소 내용이 변경되어서 fork를 진행한 저장소와 달라진경우.
      - 원래 저장소를 upstream의 이름으로 원격저장소로 추가해준다.
      - 추가된 upstream에서 우클릭 한 후 "upstream에서 가져와 병합하기"를 선택한다.
      - 원격 저장소의 내용이 Local Repository에 반영된다.
      - 이 내용을 commit & push하여 내 원격 저장소에도 반영해 주면 완료.
 - Private한 경우
   : 프로젝트의 주인이 계정을 열람권한을 주어야 프로젝트를 사용할 수 있다.
   그 이외에는 위와 동일하다.
